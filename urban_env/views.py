import json
from collections import defaultdict

from django.shortcuts import render
from django.views.generic import TemplateView, FormView

from urban_env.forms import IndexForm
from urban_env.models import Train, TrainClasses, TrainFeel, TrainFeelClasses
from urban_env.utils import InstagramService
from urban_env.utils.other import get_address, get_addresses
from urban_env.utils.train import get_features, classify
from urban_env.yandex_geocoder import Client
from hack_ai.settings import active_vk_sessions
from geopy.geocoders import Nominatim

from src.social.vk import my_vk as vk


class IndexView(FormView):
    template_name = 'urban_env/index.html'
    form_class = IndexForm

    @staticmethod
    def get_url_address(link):
        url_address = ''
        if link == 1:
            url_address = 'https://vk.com/pro_cheby'
        elif link == 2:
            url_address = 'https://vk.com/yumorxxl'
        elif link == 4:
            url_address = 'https://vk.com/progorod21'
        elif link == 5:
            url_address = 'https://www.instagram.com/progorod21/'
        else:
            print('popka')
        return url_address

    @staticmethod
    def nb_comments(comments, model_train, model_classes):
        # Список всех фич из всех комментариев
        model = model_train.objects.all()
        model_classes = json.loads(model_classes.objects.get().classes)
        prob = defaultdict(lambda: 0)
        for obj in model:
            prob[obj.label, obj.word] = int(obj.freq.replace('.0', ''))

        model_feel = TrainFeel.objects.all()
        model_classes_feel = json.loads(TrainFeelClasses.objects.get().classes)
        probfeel = defaultdict(lambda: 0)
        for obj in model_feel:
            probfeel[obj.label, obj.word] = int(obj.freq.replace('.0', ''))

        data = {}
        for comment in comments:
            label = classify((model_classes, prob), get_features(comment))
            label_feel = classify((model_classes_feel, probfeel), get_features(comment))
            if comment != '':
                address = get_address(comment)
                if label != 'other' and address is not None:
                    data[address] = (label, label_feel, comment)

        return data

    def post(self, request, *args, **kwargs):
        # Вызов логики

        request_count = 100
        link = int(request.POST['link'])
        url_address = self.get_url_address(link)
        comments = []
        if link in [1, 2, 4]:
            url_address = url_address.replace("https://vk.com/", "")
            comments = vk.get_group_vk_comments(active_vk_sessions, url_address, request_count)
        elif link == 5:
            service = InstagramService()
            comments = service.execute('https://www.instagram.com/progorod21/', 5)

        address = self.nb_comments(comments, Train, TrainClasses)

        return render(
            request,
            'urban_env/map.html',
            {
                'address': get_addresses(address)
            }
        )


class MapView(TemplateView):
    template_name = 'urban_env/map.html'

    def get_context_data(self, **kwargs):
        context = super(MapView, self).get_context_data(**kwargs)
        # Тестовые данные
        address = {
            'Чебоксары Ул.афанасьева д.13': ('infrastructure', 'normal', 'текст'),
            'Чебоксары Проспект 9-ой пятилетки': ('communal_services', 'normal', 'текст'),
            'Чебоксары Асламаса 11': ('other', 'normal', 'текст'),
            'Чебоксары Президентский бульвар, 5/17': ('roads', 'normal', 'текст'),
            'Чебоксары Московский проспект, 1': ('communal_services', 'normal', 'текст'),
            'Чебоксары проспект Максима Горького, 18Б': ('infrastructure', 'normal', 'текст'),
            'Чебоксары улица Афанасьева': ('infrastructure', 'normal', 'текст'),
            'Чебоксары улица Пирогова': ('roads', 'normal', 'текст'),
            'Чебоксары Асламаса 10': ('communal_services', 'normal', 'текст'),
            'Чебоксары Асламаса 9': ('communal_services', 'normal', 'текст'),
            'Чебоксары Асламаса 1': ('communal_services', 'normal', 'текст'),
            'Чебоксары ул. Ивана Франко': ('infrastructure', 'normal', 'текст'),
            'Чебоксары ул. Карла Маркса': ('infrastructure', 'normal', 'текст'),
            'Чебоксары ул. Гагарина': ('infrastructure', 'normal', 'текст'),
        }
        context['address'] = get_addresses(address)
        return context
