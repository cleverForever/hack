# 0-положительно 1 - отрицательно
def corpus_write(data_set, path0, path1, path2, path3):
    """
    Создание корпуса данных
    :param data_set: список списков (коммент, лейбл)
    :return:
    """
    lst_infrastructure, lst_roads, lst_communal_services, lst_other = [], [], [], []

    for i in data_set:
        if i[1] == 1:
            lst_infrastructure.append(i[0] + '\n')
        elif i[1] == 2:
            lst_roads.append(i[0] + '\n')
        elif i[1] == 3:
            lst_communal_services.append(i[0] + '\n')
        else:
            lst_other.append(i[0] + '\n')

    for i in range(len(lst_infrastructure)):
        infrastructure = open(path0 + str(i) + '.txt', 'w', encoding='utf-8')
        infrastructure.writelines(lst_infrastructure[:1])
        del lst_infrastructure[:1]

    for r in range(len(lst_roads)):
        roads = open(path1 + str(r) + '.txt', 'w', encoding='utf-8')
        roads.writelines(lst_roads[:1])
        del lst_roads[:1]

    for c in range(len(lst_communal_services)):
        communal_services = open(path2 + str(c) + '.txt', 'w', encoding='utf-8')
        communal_services.writelines(lst_communal_services[:1])
        del lst_communal_services[:1]

    for o in range(len(lst_other)):
        other = open(path3 + str(0) + '.txt', 'w', encoding='utf-8')
        other.writelines(lst_other[:1])
        del lst_other[:1]


def export_labeled():
    # берем только негативные комменты
    with open('urban_env//data//labeled.csv', 'r', encoding='utf-8') as file_dataset:
        comments = file_dataset.readlines()

    data_set = []
    for c in comments:
        c = c.replace('"', '')
        c = c.replace('"', '')
        c = c.replace('.0', '')
        c = c.replace('\n', '')
        if c == ',1' or c == ',0':
            c = c[1]
        data_set.append(c)

    need = []
    for data in range(0, len(data_set), 2):
        lst = [data_set[data], int(data_set[data + 1])]
        need.append(lst)

    lst = [i for i in need]
    return lst


def corpus_write_feel(data_set, path0, path1):
    """
    Создание корпуса данных
    :param data_set: список списков (коммент, лейбл)
    :return:
    """
    lst_normal = []
    lst_bad = []

    for i in data_set:
        if i[1] == 0:
            lst_normal.append(i[0] + '\n')
        else:
            lst_bad.append(i[0] + '\n')
    for n in range(971):
        normal = open(path0 + str(n) + '.txt', 'w', encoding='utf-8')
        normal.writelines(lst_normal[:8])
        del lst_normal[:8]

    for b in range(559):
        bad = open(path1 + str(b) + '.txt', 'w', encoding='utf-8')
        bad.writelines(lst_bad[:8])
        del lst_bad[:8]