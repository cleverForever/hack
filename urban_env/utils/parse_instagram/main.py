from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time, urllib.request
import requests
from selenium.webdriver.remote.webelement import WebElement

from hack_ai.settings import CHROMEDRIVER_PATH


class InstagramService:
    def __init__(self):
        """
        Инициализация сервиса
        """
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('window-size=1920x935')
        self.driver = webdriver.Chrome(CHROMEDRIVER_PATH, chrome_options=options)

    def _login(self) -> None:
        """
        Инициализация сервиса Инстаграм
        """
        # Заходим на сайт инсты
        self.driver.get("https://www.instagram.com/")
        time.sleep(3)

        # Логинимся
        username = self.driver.find_element(By.CSS_SELECTOR, "input[name='username']")
        password = self.driver.find_element(By.CSS_SELECTOR, "input[name='password']")
        username.clear()
        password.clear()
        username.send_keys("ardrzhn2000_")
        password.send_keys("ardrzhn2000_!")
        self.driver.find_element(By.CSS_SELECTOR, "button[type='submit']").click()
        time.sleep(4)

    def _open_page(self, url_account: str) -> None:
        """
        Открываем страницу

        :param url_account: URl-адрес аккаунта
        """
        self.driver.get(url_account)

    def _get_comments_from_posts(self, count_posts: int) -> list[str]:
        """
        Открываем пост

        :param count_posts: количество постов для парсинга
        :return: Комментарии со всех постов
        """
        comments = []
        i = 0
        count_comments = len(self.driver.find_elements(By.CLASS_NAME, "_9AhH0"))

        # Смотрим количество постов у юзера
        # Если больше, чем нужно парсить - выходим
        while i < count_comments and i < count_posts:
            # Открываем пост
            self.driver.find_elements(By.CLASS_NAME, "_9AhH0")[i].click()
            time.sleep(1)
            self._open_comments()
            self._open_subcomments()
            comments.extend(self._get_comments())

            # Закрываем пост
            self.driver.find_elements(By.CLASS_NAME, "qJPeX")[0].click()
            time.sleep(0.5)

            i += 1
            count_comments = len(self.driver.find_elements(By.CLASS_NAME, "_9AhH0"))

        return comments

    def _get_last_and_predlast_comments(self, last_comment_url: str, delay: float) -> tuple[str, str]:
        """
        Делаем предпоследний комментарий равным последнему, загружаем ещё комментарии, получаем последний комментарий

        :param last_comment_url: последний комментарий
        :param delay: задержка выполнения
        :return: предпоследний комментарий, последний комментарий
        """
        pred_last_comment_url = last_comment_url
        try:
            self.driver.find_element(By.CSS_SELECTOR, "svg[aria-label='Загрузить ещё комментарии']").click()
            time.sleep(delay)
            last_comment_url = self.driver.find_elements(By.CLASS_NAME, "gU-I7")[-1].get_attribute("href")
        finally:
            return pred_last_comment_url, last_comment_url

    def _open_comments(self) -> None:
        """
        Раскрываем все комментарии
        """
        try:
            pred_last_comment_url = self.driver.find_elements(By.CLASS_NAME, "gU-I7")[-1].get_attribute("href")
            last_comment_url = ''
            self.driver.find_element(By.CSS_SELECTOR, "svg[aria-label='Загрузить ещё комментарии']")
        except:
            return

        while pred_last_comment_url != last_comment_url:
            try:
                # Место разрыва нажатия на кнопку загрузки комментариев, когда все комменты загружены
                pred_last_comment_url, last_comment_url = self._get_last_and_predlast_comments(last_comment_url, 0.2)
                if pred_last_comment_url == last_comment_url:
                    pred_last_comment_url, last_comment_url = self._get_last_and_predlast_comments(last_comment_url, 1)
            except:
                continue

    def _open_subcomments(self) -> None:
        """
        Раскрываем ответы на комментарии
        """
        all_hide_comments = self.driver.find_elements(By.CLASS_NAME, "EizgU")
        while all_hide_comments:
            while all_hide_comments[0].text.find('Посмотреть') >= 0:
                all_hide_comments[0].click()
                time.sleep(0.2)
            del all_hide_comments[0]

    def _get_comments(self) -> list[str]:
        """
        Получаем все комментарии

        :param comments: список элементов(необработанных комментариев)
        :return: список текстов комментариев
        """
        comments = []
        for comment in self.driver.find_elements(By.CLASS_NAME, "C4VMK"):
            comments.append(comment.find_elements(By.TAG_NAME, 'span')[1].text)
        return comments

    def execute(self, url_account: str, count_posts: int = 10) -> list[str]:
        """
        Парсинг Инстаграма

        :param url_account: URl-адрес аккаунта
        :param count_posts: количество постов для парсинга
        :return: список текстов комментариев
        """
        self._login()
        self._open_page(url_account)
        comments = self._get_comments_from_posts(count_posts)
        return comments
