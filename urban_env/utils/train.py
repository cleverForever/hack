from collections import defaultdict
from math import log

from nltk.corpus import PlaintextCorpusReader
from nltk.stem.snowball import SnowballStemmer
from nltk import RegexpTokenizer
from nltk import bigrams
from nltk import FreqDist
from nltk import pos_tag


def lower_pos_tag(words):
    lower_words = []
    for i in words:
        lower_words.append(i.lower())
    pos_words = pos_tag(lower_words, lang='rus')
    return pos_words


def clean(words):
    stemmer = SnowballStemmer("russian")
    cleaned_words = []
    for i in words:
        if i[1] in ['S', 'A', 'V', 'ADV']:
            cleaned_words.append(stemmer.stem(i[0]))
    return cleaned_words


corpus_root = 'urban_env//data//feel'  # Путь к корпусу


def process(label):
    # Промежуточный список для удаления гапаксов
    templist_allwords = []
    # Определение пути к папке с определенным лейблом
    corpus = PlaintextCorpusReader(corpus_root + '\\' + label, '.*', encoding='utf-8')
    # Получение списка имен файлов в корпусе
    names = corpus.fileids()

    # Создание токенайзера
    tokenizer = RegexpTokenizer(r'\w+|[^\w\s]+')
    for name in names:  # Обработка корпуса
        print(name)
        bag_words = tokenizer.tokenize(corpus.raw(name))
        lower_words = lower_pos_tag(bag_words)
        cleaned_words = clean(lower_words)
        templist_allwords.extend(cleaned_words)

    # Определение гапаксов
    temp_list_freq = FreqDist(templist_allwords)
    hapaxes = temp_list_freq.hapaxes()
    # Фильтрация от гапаксов
    count = 0
    lst = []
    for word in templist_allwords:
        count += 1
        print(count)
        if word not in hapaxes:
            lst.append(word)
    return {label: lst}


def get_features(words):
    words = words.split(' ')
    lower_words = []
    for i in words:
        lower_words.append(i.lower())
    pos_words = pos_tag(lower_words, lang='rus')

    stemmer = SnowballStemmer("russian")
    cleaned_words = []
    for i in pos_words:
        if i[1] in ['S', 'A', 'V', 'ADV']:
            cleaned_words.append(stemmer.stem(i[0]))
    return cleaned_words


def classify(classifier, feats):
    classes, prob = classifier
    return min(classes.keys(),  # calculate argmin(-log(C|O))
               key=lambda cl: -log(classes[cl]) + \
                              sum(-log(
                                  prob.get(
                                      (cl, feat), 10 ** (-7)
                                  )
                              ) for feat in feats)
               )


# Методы для самого обучения
def train(data):
    classes, freq = defaultdict(lambda: 0), defaultdict(lambda: 0)
    for label, vec_feat in data.items():
        classes[label] += 1  # count classes frequencies
        for feat in vec_feat:
            freq[label, feat] += 1  # count features frequencies

    len_count = len(data['infrastructure']) + len(data['roads']) + len(data['communal_services']) + len(data['other'])
    for label, feat in freq:  # normalize features frequencies
        freq[label, feat] /= classes[label]
    for c in classes:  # normalize classes frequencies
        classes[c] /= len_count

    return classes, freq  # return P(C) and P(O|C)


# Методы для самого обучения
def train_feel(data):
    # Создание вокабуляра с уникальными лексемами
    all_words_bad, all_words_normal = {}, {}
    classes, freq = defaultdict(lambda: 0), defaultdict(lambda: 0)
    for label, vec_feat in data.items():
        classes[label] += 1  # count classes frequencies
        for feat in vec_feat:
            freq[label, feat] += 1  # count features frequencies

    len_count = len(data['normal']) + len(data['bad'])
    for label, feat in freq:                # normalize features frequencies
        freq[label, feat] /= classes[label]
    for c in classes:                       # normalize classes frequencies
        classes[c] /= len_count

    return classes, freq                    # return P(C) and P(O|C)
