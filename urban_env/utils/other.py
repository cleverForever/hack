import json
import re

from geopy import Nominatim

from urban_env.const import CHEBOKSARY


def get_address(comment) -> str:
    comment = comment.split(' ')
    words = [word.lower() for word in comment]
    streets = [street.lower() for street in CHEBOKSARY]
    address = 'Чебоксары '
    for word in words:
        word = word.replace('ул.', '')
        word = word.replace('ул', '')


        if word in streets:
            address += word
        else:
            continue

    if len(address) == 10:
        address = None

    home = 0
    for word in words:
        word = word.replace('д.', '')
        word = word.replace('д', '')
        try:
            home = int(word)
        except ValueError:
            continue
    if home != 0 and home < 111 and address is not None:
        address += (' ' + str(home))

    return address


def get_addresses(address):
    geo = Nominatim(user_agent="my_request")

    # Словарь - { Адрес: [[координаты], тип улучшения, окраска] }
    coordinates = dict()
    for key, value in address.items():
        location = geo.geocode(key)
        if location:
            coordinates[key] = [[location.longitude, location.latitude], value[0], value[1], value[2]]

        # Для яндекса
        # Привожу к float, так как изначально Decimal
        # coord = [float(x) for x in client.coordinates(key)]
        # coordinates[key] = [coord, value]

    address = json.dumps(coordinates)
    return address
