from openpyxl import load_workbook
from openpyxl.worksheet._read_only import ReadOnlyWorksheet

from urban_env import const


class ImportExcelService:
    def _load(self, file_name: str) -> ReadOnlyWorksheet:
        """
        Открытие файла

        :param file_name: Расположение файла
        :return: Активная страница из книги Excel
        :raise FileExistsError: Проблема в открытии файла
        """
        # Чтение файла
        wb = load_workbook(file_name, read_only=True)

        # Выбор активной страницы из книги Excel
        ws = wb.active
        return ws

    def _parse_generator(self, ws: ReadOnlyWorksheet) -> tuple:
        """
        Парсинг файла

        :param ws: Активная страница из книги Excel
        :return: Генератор кортежей вида (Текст обращения, Категория, Муниципалитет)
        """
        # Пропуск первой строки(служебной информации)
        iter_rows = ws.iter_rows()
        next(iter_rows)
        for row in iter_rows:
            # Пустые значения не должны учитываться
            if not row[5].value or not row[3].value:
                continue
            yield row[5].value, const.CATEGORIES[row[3].value]

    def execute(self, file_name: str) -> list[tuple]:
        """
        Парсинг файла

        :param file_name: Расположение файла
        :return: Список, состоящий из кортежей вида (Текст обращения, Категория, Муниципалитет)
        """
        ws = self._load(file_name)
        return [tpl for tpl in self._parse_generator(ws)]
