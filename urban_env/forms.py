from django import forms

CHOICE = (
    (0, '----------'),
    (1, 'ПОДСЛУШАНО ЧЕБОКСАРЫ | НОВОСТИ'),
    (2, 'Подслушано Чебоксары'),
    (4, 'Про Город Чебоксары | Новочебоксарск | Чувашия'),
    (5, 'progorod21'),
)


class IndexForm(forms.Form):
    link = forms.ChoiceField(label='', required=True, choices=CHOICE)
