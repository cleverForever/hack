from django.db import models


# Create your models here.
class Train(models.Model):
    label = models.TextField(verbose_name='label')
    word = models.TextField(verbose_name='word')
    freq = models.TextField(verbose_name='freq')


class TrainClasses(models.Model):
    classes = models.TextField(verbose_name='P(C)')


class TrainFeel(models.Model):
    label = models.TextField(verbose_name='label')
    word = models.TextField(verbose_name='word')
    freq = models.TextField(verbose_name='freq')


class TrainFeelClasses(models.Model):
    classes = models.TextField(verbose_name='P(C)')
